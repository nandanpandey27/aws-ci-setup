terraform {
  required_version = ">= 0.12"

  backend "s3" {
    bucket         = "aws-setup-ci-tfstate"
    key            = "aws-setup-ci.tfstate"
    region         = "ap-south-1"
    encrypt        = true
    dynamodb_table = "aws-ci-setup-tfstate-lock"
  }
}

provider "aws" {
  region  = "ap-south-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

