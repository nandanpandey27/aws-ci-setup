variable "prefix" {
  default = "acs"
}

variable "project" {
  default = "aws-ci-setup-devops"
}

variable "contact" {
  default = "nandanpandey@outlook.com"
}