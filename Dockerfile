FROM node:stretch

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 6000

ENV PORT=6000

CMD ["npm", "start"]